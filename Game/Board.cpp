#include "StdAfx.h"
#include "Board.h"

Board::Board(int width) {

  setWidthOfCells(width);
  
  POINT places[rows] = {
    {3, 1},
    {1, 3}, {3, 3}, {5, 3},
    {1, 5}, {3, 5}, {5, 5},
    {1, 7}, {3, 7}, {5, 7},
    {3, 9}
  };

  cells = new Cell[countOfCells];
  for (int i = 0; i < countOfCells; i++) {
    cells[i].setWidth(widthOfCells);

    cells[i].setX(cells[i].getWidth() * places[i].x);
    cells[i].setY(cells[i].getWidth() * places[i].y);
  }
//  makeMap();
}

Board::~Board(void) {
  delete [] cells;
}

void Board::print(CDC *pDC) {
  POINT relations[countOfRelations] = {
    {0, 1}, {0, 2}, {0, 3},
    {1, 2}, {1, 5}, {1, 4},
    {2, 5}, {2, 3},
    {3, 5}, {3, 6},
    {4, 5}, {4, 7},
    {5, 7}, {5, 8}, {5, 9},
    {6, 5}, {6, 9},
    {7, 8}, {7, 10},
    {8, 9}, {8, 10},
    {9, 10}
  };

  CPen * oldPen;
  CPen thick_pen(PS_SOLID, 3, RGB(0,255,0));
  oldPen = pDC->SelectObject(&thick_pen);
  
  int halfOfWidthOfCells = getWidthOfCells() / 2;
  for (int i = 0; i < countOfRelations; i++) {
    pDC->MoveTo(cells[relations[i].x].getX() + halfOfWidthOfCells,
                cells[relations[i].x].getY() + halfOfWidthOfCells);

    pDC->LineTo(cells[relations[i].y].getX() + halfOfWidthOfCells,
                cells[relations[i].y].getY() + halfOfWidthOfCells);
  }
  pDC->SelectObject(oldPen);
  
  for (int i = 0; i < rows; i++) {
    cells[i].print(pDC);
  }

//  printf("Board::print\n");
}

int Board::getRows() {
  return rows;
}

int Board::getCols() {
  return cols;
}

int Board::getWidthOfCells() {
  return widthOfCells;
}

void Board::setWidthOfCells(int value) {
  widthOfCells = value;
}
