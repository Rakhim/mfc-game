﻿// BoardWnd.cpp: файл реализации
//

#include "stdafx.h"
#include "Game.h"
#include "BoardWnd.h"

// CBoardWnd

IMPLEMENT_DYNAMIC(CBoardWnd, CWnd)

CBoardWnd::CBoardWnd(CGameView *pView)
{
  this->pView = pView;
  this->pDoc = pView->GetDocument();
}

CBoardWnd::~CBoardWnd()
{
}


BEGIN_MESSAGE_MAP(CBoardWnd, CWnd)
  ON_WM_MOUSEMOVE()
  ON_WM_LBUTTONDOWN()
  ON_WM_CREATE()
//  ON_WM_SIZE()
END_MESSAGE_MAP()



// обработчики сообщений CBoardWnd


void CBoardWnd::OnMouseMove(UINT nFlags, CPoint point)
{
  ASSERT_VALID(pDoc);
//printf("CBoardWnd::OnMouseMove\n");
  Coeff coeff = getCoefficients(&point);
  printf("[%3d, %3d] ~ [%3f, %3f]\n", point.x, point.y, 
    coeff.x, coeff.y);
  Cell *pCell = pDoc->getCell(point);
  if (pCell) {
    pCell->setBackgroundColor(0x00FF00);
    pView->updateBoardView(pCell);
//    RECT r;
//    r = pCell->getRectangle();
    pView->InvalidateRect(&pCell->getRectangle());
  }

  CWnd::OnMouseMove(nFlags, point);
}


void CBoardWnd::OnLButtonDown(UINT nFlags, CPoint point)
{
  ASSERT_VALID(pDoc);
//  pDoc->test(point.x, point.y);
//  printf("CBoardWnd::OnLButtonDown\n");
  CWnd::OnLButtonDown(nFlags, point);
}


int CBoardWnd::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
  if (CWnd::OnCreate(lpCreateStruct) == -1)
    return -1;
//  ASSERT_VALID(pDoc);
//  pDoc->makeMap(GetWindow(0));
  
  return 0;
}

//  
//void CBoardWnd::OnSize(UINT nType, int cx, int cy)
//{
//  CWnd::OnSize(nType, cx, cy);
//  //{
//  //  RECT r;
//  //  GetClientRect(&r);
//  //  printf("%15s (%s) : [%3d, %3d]\n", "ActualClient", "CBoard", r.right - r.left, r.bottom - r.top);
//  //}
//  //printf("CBoardWnd::OnSize %5d %5d\n", cx, cy);
//}
