//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Game.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_GameTYPE                    130
#define IDR_TOOLBAR                     310
#define ID_32778                        32778
#define ID_32779                        32779
#define ID_32780                        32780
#define ID_32781                        32781
#define ID_VIEW_STATUSBAR               32782
#define IDM_TOOLBAR                     32783
#define IDM_STATUSBAR                   32784
#define IDM_APP_ABOUT                   32785
#define IDM_COLORS                      32786
#define IDM_HISTORY                     32787
#define IDDO                            32788

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        313
#define _APS_NEXT_COMMAND_VALUE         32789
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           312
#endif
#endif
