// GameView.h : ��������� ������ CGameView
//

#pragma once
#include "Offscreen.h"
#include "GameDoc.h"

class CGameView : public CView
{
protected: // ������� ������ �� ������������
	CGameView();
	DECLARE_DYNCREATE(CGameView)

// ��������
public:
 	CGameDoc* GetDocument() const;
private:  
  Offscreen *pOffscreen;
  CWnd* pBoardWnd;
// ��������
private:

public:
  void updateBoardView(Cell *pCell) {
    GetDocument()->write(pOffscreen);
  }
// ���������������
public:
	virtual void OnDraw(CDC* pDC);  // �������������� ��� ��������� ����� �������������
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
  virtual void OnInitialUpdate();
  virtual BOOL OnEraseBkgnd(CDC* pDC);
  virtual void OnSize(UINT nType, int cx, int cy);
protected:
  
// ����������
public:
	virtual ~CGameView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// ��������� ������� ����� ���������
protected:
	DECLARE_MESSAGE_MAP()
public:
  afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
  afx_msg void OnDestroy();
};

#ifndef _DEBUG  // ���������� ������ � GameView.cpp
inline CGameDoc* CGameView::GetDocument() const
   { return reinterpret_cast<CGameDoc*>(m_pDocument); }
#endif