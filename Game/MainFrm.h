
// MainFrm.h : ��������� ������ CMainFrame
//

#pragma once
#include "GameDoc.h"

class CMainFrame : public CFrameWnd
{
	
protected: // ������� ������ �� ������������
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)
  
// ��������
private:
	CToolBar   toolBar;
	CStatusBar statusBar;
// ��������
public:
  
// ���������������
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
  virtual void OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI);

// ����������
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// ��������� ������� ����� ���������
protected:
	DECLARE_MESSAGE_MAP()

public:
  afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
};


