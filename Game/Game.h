
// Game.h : ������� ���� ��������� ��� ���������� Game
//
#pragma once

#ifndef __AFXWIN_H__
	#error "�������� stdafx.h �� ��������� ����� ����� � PCH"
#endif

#include "resource.h"       // �������� �������


// CGameApp:
// � ���������� ������� ������ ��. Game.cpp
//

class CGameApp : public CWinApp
{
public:
	CGameApp();

// ���������������
public:
	virtual BOOL InitInstance();
  virtual BOOL ExitInstance();
// ����������
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern CGameApp theApp;
