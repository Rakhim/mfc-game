#pragma once
#include "Printable.h"

class Cell: public Printable
{
  static const int penWidth = 3;

  int width;
  COLORREF bgColor;
  COLORREF borderColor;

  POINT position;
  POINT relative;
  
  Cell *child;
public:
  Cell();
  Cell(int x, int y, int width);
  ~Cell(void);
  
  COLORREF getBackgroundColor();
  void setBackgroundColor(COLORREF color);

  COLORREF getBorderColor();
  void setBorderColor(COLORREF color);

  int getX();
  void setX(int);

  int getY();
  void setY(int);

  int getWidth();
  void setWidth(int);

  bool isInner(int x, int y);
  
  
  Cell* getChild();
  void setChild(Cell *cell);
  void unsetChild();

  RECT getRectangle();
  RECT getAbsoluteRectangle();
  // realization
public:
  void print(CDC *pDC);
  void print(CDC *pDC, COLORREF borderColor, COLORREF backgroundColor);
private:
  
  int getRelativeX();
  void setRelativeX(int);

  int getRelativeY();
  void setRelativeY(int);
};
