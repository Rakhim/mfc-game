#include "StdAfx.h"
#include "Cell.h"

Cell::Cell(): 
  bgColor(0),
  borderColor(0),
  width(0)
{
  setRelativeX(0);
  setRelativeY(0);

  setX(0);
  setY(0);
}

Cell::Cell(int x, int y, int width) {
  setRelativeX(0);
  setRelativeY(0);
  
  setX(x);
  setY(y);
  
  setWidth(width);
}

Cell::~Cell(void) {
}

int Cell::getWidth() {
  return width;
}

void Cell::setWidth(int value) {
  width = value;
}

int Cell::getX() {
  return position.x;
}

void Cell::setX(int value) {
  position.x = value;
}

int Cell::getY() {
  return position.y;
}

void Cell::setY(int value) {
  position.y = value;
}

Cell* Cell::getChild() {
  return child;
}

void Cell::setChild(Cell *cell) {
  if (this == cell) return;
  child = cell;

  child->setRelativeX(getX());
  child->setRelativeY(getY());
}

RECT Cell::getRectangle() {
  RECT rect;
  rect.left = position.x;
  rect.top = position.y;
  rect.right = rect.left + width;
  rect.bottom = rect.top + width;
  return rect;
}

RECT Cell::getAbsoluteRectangle() {
  RECT rect;
  rect.left = relative.x + position.x;
  rect.top = relative.y + position.y;
  rect.right = rect.left + width;
  rect.bottom = rect.top + width;
  return rect;
}

void Cell::print(CDC *pDC) {
  CPen * oldPen;
  CPen thick_pen(PS_SOLID, penWidth, getBorderColor());
  oldPen = pDC->SelectObject(&thick_pen);
  
  CBrush * oldBrush;
  CBrush brush(getBackgroundColor());

  oldBrush = pDC->SelectObject(&brush);

  pDC->Ellipse(&getRectangle());

  pDC->SelectObject(oldBrush);
  pDC->SelectObject(oldPen);
}

void Cell::print(CDC *pDC, COLORREF borderColor, COLORREF backgroundColor) {
  CPen * oldPen;
  CPen thick_pen(PS_SOLID, penWidth, borderColor);
  oldPen = pDC->SelectObject(&thick_pen);
  
  CBrush * oldBrush;
  CBrush brush(backgroundColor);

  oldBrush = pDC->SelectObject(&brush);

  pDC->Ellipse(&getRectangle());

  pDC->SelectObject(oldBrush);
  pDC->SelectObject(oldPen);
}

int Cell::getRelativeX() {
  return relative.x;
}
void Cell::setRelativeX(int value) {
  relative.x = value;
}

int Cell::getRelativeY() {
  return relative.y;
}

void Cell::setRelativeY(int value) {
  relative.y = value;
}

void Cell::unsetChild() {
  child->setRelativeX(0);
  child->setRelativeY(0);
  child = NULL;
}

COLORREF Cell::getBackgroundColor() {
  return bgColor;
}

void Cell::setBackgroundColor(COLORREF color) {
  bgColor = color;
}

COLORREF Cell::getBorderColor() {
  return borderColor;
}

void Cell::setBorderColor(COLORREF color) {
  borderColor = color;
}

bool Cell::isInner(int x, int y) {
  double radius = (width / 2);
  RECT r = getRectangle();
  return !(radius * radius <
    (position.x - x) * (position.x - x) + 
    (position.y - y) * (position.y - y)
  );
}