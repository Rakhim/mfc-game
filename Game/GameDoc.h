
// GameDoc.h : ��������� ������ CGameDoc
//


#pragma once
#include "Offscreen.h"
#include "Board.h"

class CGameDoc : public CDocument
{
protected: // ������� ������ �� ������������
	CGameDoc();
	DECLARE_DYNCREATE(CGameDoc)

// ��������
private:
  Board *pBoard;

// ��������
public:

  //void test(int x, int y) {
  //  pBoard->test(x, y);
  //}
  //void makeMap(CWnd *wnd) {
  //  pBoard->makeMap(wnd);
  //}  

  void write(Offscreen *offscreen) {
    offscreen->write(pBoard);
  }

  size_t getWidth() {
    return pBoard->getCols() * pBoard->getWidthOfCells();
  }

  size_t getHeight() {
    return pBoard->getRows() * pBoard->getWidthOfCells();
  }
  
  Cell* getCell(CPoint point) {
    return pBoard->getCellId(point.x, point.y);
  }

  void update(CPoint pCPoint) {
    
  }
// ���������������
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
#ifdef SHARED_HANDLERS
	virtual void InitializeSearchContent();
	virtual void OnDrawThumbnail(CDC& dc, LPRECT lprcBounds);
#endif // SHARED_HANDLERS

// ����������
public:
	virtual ~CGameDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// ��������� ������� ����� ���������
protected:
	DECLARE_MESSAGE_MAP()

#ifdef SHARED_HANDLERS
	// ��������������� �������, �������� ���������� ������ ��� ����������� ������
	void SetSearchContent(const CString& value);
#endif // SHARED_HANDLERS
};
