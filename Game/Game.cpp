// Game.cpp : ���������� ��������� ������� ��� ����������.
//

#include "stdafx.h"
#include "afxwinappex.h"
#include "afxdialogex.h"
#include "Game.h"
#include "MainFrm.h"

#include "GameDoc.h"
#include "GameView.h"

#include "AboutDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CGameApp

BEGIN_MESSAGE_MAP(CGameApp, CWinApp)
	ON_COMMAND(IDM_APP_ABOUT, &CGameApp::OnAppAbout)
	// ����������� ������� �� ������ � ������� ����������
	ON_COMMAND(ID_FILE_NEW, &CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, &CWinApp::OnFileOpen)
END_MESSAGE_MAP()


// �������� CGameApp

CGameApp::CGameApp()
{
	// TODO: �������� ���� ������ �������������� ���������� ������� ����������� ��������������; �������������
	// ������ ��� ������: �����������.�����������.����������.���������������
	SetAppID(_T("company.app.1.0"));

	// TODO: �������� ��� ��������,
	// ��������� ���� ������ ��� ������������� � InitInstance
}

// ������������ ������ CGameApp

CGameApp theApp;


// ������������� CGameApp

BOOL CGameApp::InitInstance()
{

#ifdef _DEBUG
  if (!AllocConsole()) {
    MessageBox(NULL, L"Failed to create the console!", L"caption",MB_ICONERROR);
  }
  
  freopen("CONOUT$", "w", stdout);  
#endif

  { // Sure about only one existence of instance

    CreateMutex(NULL, FALSE, TEXT("{5A9FEE3A-C592-40E8-8AB1-2C1AAC7302E3}"));
    if (ERROR_ALREADY_EXISTS == GetLastError()) {
      MessageBox(NULL, L"Only one instance is allowed. ", L"", MB_ICONERROR);
      return FALSE;
    }
  }

	// InitCommonControlsEx() ��������� ��� Windows XP, ���� ��������
	// ���������� ���������� ComCtl32.dll ������ 6 ��� ����� ������� ������ ��� ���������
	// ������ �����������. � ��������� ������ ����� ��������� ���� ��� �������� ������ ����.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// �������� ���� �������� ��� ��������� ���� ����� ������� ����������, ������� ���������� ������������
	// � ����� ����������.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	EnableTaskbarInteraction(FALSE);

	// ��� ������������� �������� ���������� RichEdit ��������� ����� AfxInitRichEdit2()	
	// AfxInitRichEdit2();

	// ����������� �������������
	// ���� ��� ����������� �� ������������ � ���������� ��������� ������
	// ��������� ������������ �����, ���������� ������� �� ����������
	// ���������� ��������� �������������, ������� �� ���������
	// �������� ������ �������, � ������� �������� ���������
	// TODO: ������� �������� ��� ������ �� ���-������ ����������,
	// �������� �� �������� �����������
	SetRegistryKey(_T("��������� ����������, ��������� � ������� ������� ����������"));
	LoadStdProfileSettings(0);  // ��������� ����������� ��������� INI-����� (������� MRU)


	// ��������������� ������� ���������� ����������. ������� ����������
	//  ��������� � ���� ���������� ����� �����������, ������ ����� � ���������������
	CSingleDocTemplate* pDocTemplate;
	pDocTemplate = new CSingleDocTemplate(
		IDR_MAINFRAME,
		RUNTIME_CLASS(CGameDoc),
		RUNTIME_CLASS(CMainFrame),       // �������� ���� ����� SDI
		RUNTIME_CLASS(CGameView));
	if (!pDocTemplate)
		return FALSE;
	AddDocTemplate(pDocTemplate);

	// �������������� ������ ��������� ������ �� ����������� ������� ��������, DDE, �������� ������
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);

	// �������� �������� ���������� DDE
	EnableShellOpen();
	RegisterShellFileTypes(TRUE);

	// ������� ���������������, ��������� � ��������� ������. �������� FALSE ����� ����������, ����
	// ���������� ���� �������� � ���������� /RegServer, /Register, /Unregserver ��� /Unregister.
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;

	// ���� � ������ ���� ���� ���� ����������������, ������� ���������� � �������� ���
  CString appTitle;
	appTitle.LoadStringW(AFX_IDS_APP_TITLE);
	m_pMainWnd->SetWindowTextW(appTitle.GetString());

	m_pMainWnd->ShowWindow(SW_SHOW);
	m_pMainWnd->UpdateWindow();
	// ����� DragAcceptFiles ������ ��� ������� ��������
	//  � ���������� SDI ��� ������ ��������� ����� ProcessShellCommand
	// �������� �������� ��������������
	m_pMainWnd->DragAcceptFiles();
	return TRUE;
}

BOOL CGameApp::ExitInstance() {
  FreeConsole();
  return TRUE;
}
// ����������� ��������� CGameApp



void CGameApp::OnAppAbout()
{
	CAboutDialog aboutDlg;
	aboutDlg.DoModal();
}

// ����������� ��������� CGameApp