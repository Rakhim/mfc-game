#pragma once

#include "GameView.h"

// CBoardWnd
struct Coeff {
  double x;
  double y;
};

class CBoardWnd : public CWnd
{
	DECLARE_DYNAMIC(CBoardWnd)

  CGameDoc *pDoc;
  CGameView *pView;
public:
	CBoardWnd(CGameView *pDoc);
	virtual ~CBoardWnd();

private:
  Coeff getCoefficients(CPoint *point) {
    ASSERT_VALID(pDoc);
    Coeff coeff;
    RECT rcClient;
    GetClientRect(&rcClient);
    
    coeff.x = ((double)pDoc->getWidth() * point->x) / double(rcClient.right - rcClient.left);
    coeff.y = ((double)pDoc->getHeight() * point->y) / double(rcClient.bottom - rcClient.top);
    
    return coeff;
  }
protected:
	DECLARE_MESSAGE_MAP()
public:
  afx_msg void OnMouseMove(UINT nFlags, CPoint point);
  afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
  afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
//  afx_msg void OnSize(UINT nType, int cx, int cy);
};


