﻿
// GameView.cpp : реализация класса CGameView
//

#include "stdafx.h"
// SHARED_HANDLERS можно определить в обработчиках фильтров просмотра реализации проекта ATL, эскизов
// и поиска; позволяет совместно использовать код документа в данным проекте.
#ifndef SHARED_HANDLERS
#include "Game.h"
#endif

#include "GameDoc.h"
#include "GameView.h"
#include "BoardWnd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CGameView

IMPLEMENT_DYNCREATE(CGameView, CView)

BEGIN_MESSAGE_MAP(CGameView, CView)
  ON_WM_ERASEBKGND()
  ON_WM_SIZE()
  ON_WM_CREATE()
  ON_WM_DESTROY()
END_MESSAGE_MAP()

// создание/уничтожение CGameView

CGameView::CGameView():
  pBoardWnd(NULL),
  pOffscreen(NULL)
{
  
	// TODO: добавьте код создания
}

CGameView::~CGameView()
{
}

BOOL CGameView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: изменить класс Window или стили посредством изменения
	//  CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

// рисование CGameView
void CGameView::OnDraw(CDC* pDC)
{
//  printf("OnDraw\n");

	CGameDoc* pDoc = GetDocument();  
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;
  // TODO: добавьте здесь код отрисовки для собственных данных
  CRect rcClient;
  pBoardWnd->GetClientRect(&rcClient);
  pOffscreen->stretch(pBoardWnd->GetDC(), rcClient.right, rcClient.bottom);
}

void CGameView::OnInitialUpdate() {
//printf("CGameView::OnInitialUpdate\n");
  CGameDoc* pDoc = GetDocument();
  ASSERT_VALID(pDoc);
  if(!pDoc)
    return;
  delete pOffscreen;
  pOffscreen = new Offscreen(this->GetDC(), pDoc->getWidth(), pDoc->getHeight());
  pDoc->write(pOffscreen);

  CRect rcClient, rcWindow;
  GetClientRect(&rcClient);
  GetWindowRect(&rcWindow);
  // Calculate the difference
  int nWidthDiff = rcWindow.Width() - rcClient.Width();
  int nHeightDiff = rcWindow.Height() - rcClient.Height();

  AfxGetMainWnd()->SetWindowPos(NULL, rcClient.left,
    rcClient.top,
    nWidthDiff + pDoc->getWidth(),
    nHeightDiff + pDoc->getHeight(), SWP_NOZORDER);
}

BOOL CGameView::OnEraseBkgnd(CDC* pDC) {
  COLORREF bgcolor = RGB(0, 128, 128);
  CBrush backBrush(bgcolor);

  CBrush* pOldBrush = pDC->SelectObject(&backBrush);

  CRect rect;
  pDC->GetClipBox(&rect);

  pDC->PatBlt(rect.left, rect.top, rect.Width(), rect.Height(), PATCOPY);
  pDC->SelectObject(pOldBrush);

  return TRUE;
}

void CGameView::OnSize(UINT nType, int cx, int cy) {
//  printf("OnSize\n");

  CGameDoc* pDoc = GetDocument();
  ASSERT_VALID(pDoc);
  if(!pDoc)
    return;
  
  RECT rcClient;
  rcClient.top = 0;
  rcClient.left = 0;
  rcClient.right = pDoc->getWidth();
  rcClient.bottom = cy;
  pBoardWnd->MoveWindow(&rcClient);
  pBoardWnd->CenterWindow();
  
//  printf("%d %d", rcClient.left, rcClient.right);
}

// диагностика CGameView

#ifdef _DEBUG
void CGameView::AssertValid() const
{
	CView::AssertValid();
}

void CGameView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CGameDoc* CGameView::GetDocument() const // встроена неотлаженная версия
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CGameDoc)));
	return (CGameDoc*)m_pDocument;
}
#endif //_DEBUG


// обработчики сообщений CGameView

int CGameView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
  if (CView::OnCreate(lpCreateStruct) == -1)
    return -1;
//  printf("CGameView::OnCreate\n");

  CGameDoc* pDoc = GetDocument();
  ASSERT_VALID(pDoc);
  if(!pDoc)
    return -1;

  pBoardWnd = new CBoardWnd(this);
  pBoardWnd->Create(NULL, NULL, WS_CHILD | WS_VISIBLE | WS_BORDER,
       CRect(0, 0, pDoc->getWidth(), pDoc->getHeight()), this, 0);
//  RECT r;
//  r.
//  CalcWindowRect();
  
  return 0;
}

void CGameView::OnDestroy()
{
  CView::OnDestroy();
  delete pBoardWnd;
}
