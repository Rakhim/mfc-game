
// MainFrm.cpp : ���������� ������ CMainFrame
//

#include "stdafx.h"
#include "Game.h"

#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)  
  ON_WM_GETMINMAXINFO()
  ON_WM_CREATE()
//  ON_COMMAND(ID_FILE_NEW, &CMainFrame::on)
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // ��������� ������ ���������
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};


// ��������/����������� CMainFrame

CMainFrame::CMainFrame()
{
  // TODO: �������� ��� ������������� �����
}

CMainFrame::~CMainFrame()
{
}

void CMainFrame::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI)
{
  CGameDoc *pDoc = (CGameDoc*)GetActiveDocument();
  if(!pDoc)
    return;
  CRect rcClient, rcWindow;
  GetClientRect(&rcClient);
  GetWindowRect(&rcWindow);
  //  Calculate the difference
  int nWidthDiff = rcWindow.Width() - rcClient.Width();
  int nHeightDiff = rcWindow.Height() - rcClient.Height();

  lpMMI->ptMinTrackSize.x = pDoc->getWidth() + nWidthDiff;
  lpMMI->ptMinTrackSize.y = pDoc->getHeight() + nHeightDiff;
  
  //lpMMI->ptMaxTrackSize.x = lpMMI->ptMinTrackSize.x + pDoc->getWidth();
  //lpMMI->ptMaxTrackSize.y = lpMMI->ptMinTrackSize.y + nHeightDiff;
}


BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{  
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: �������� ����� Window ��� ����� ����������� ���������
	//  CREATESTRUCT cs

	return TRUE;
}

// ����������� CMainFrame

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}
#endif //_DEBUG


// ����������� ��������� CMainFrame


int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
  if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
    return -1;
  
  // TODO:
	if (!toolBar.CreateEx(this, TBSTYLE_AUTOSIZE,
     WS_CHILD | WS_VISIBLE | CBRS_TOP | CBRS_GRIPPER |
     CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!toolBar.LoadToolBar(IDR_TOOLBAR))
	{
#ifdef _DEBUG
  	printf("Creation of tool bar failed.\n");
#endif
		return -1;
	}
  

  if (!statusBar.Create(this)) {
#ifdef _DEBUG
  printf("Creation of status bar failed.\n");
#endif
   	return -1;
  }
  
 	statusBar.SetIndicators(indicators, sizeof(indicators)/sizeof(UINT));
	// TODO: ������� ��� ��� ������, ���� �� ����������� ���������� ������ ������������
  //EnableToolTips();
	toolBar.EnableDocking(CBRS_ALIGN_TOP);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&toolBar);

  return 0;
}
