#pragma once
#include "Cell.h"
#include "Printable.h"
#include "Offscreen.h"

class Board: public Printable
{
  static const int rows = 11;
  static const int cols = 7;
  static const int countOfCells = 11;
  static const int countOfRelations = 22;

  CDC *mapDC;
  //Offscreen *mapScreen;

  int widthOfCells;
  int width;
  int height;

  Cell *cells;

public:
  Board(int width);
  ~Board(void);

  int getWidth() {
    return width;
  }

  int getHeight() {
    return height;
  }

  void setWidth(int width) {
    this->width = width;
  }

  void setHeight() {
    this->height = height;
  }

  int getRows();
  int getCols();

  int getWidthOfCells();

  void print(CDC *pDC);                                                                                                                                                                                                                                                   
  
  Cell* getCellId(int x, int y) {
    for (int i = 0; i < countOfCells; i++) {
      if (cells[i].isInner(x, y)) {
        return (cells + i); // bad solution ?
      }
    }
    return NULL;
  }
  //  void test(int x, int y) {
  //    cells[]
  //    printf("(%d, %d) : %d\n", x, y, mapDC->GetPixel(x, y));
  //  }

  //  void makeMap(CWnd *pWnd) {  
  // mapDC = new CDC();
  //  mapScreen = new Offscreen(new CClientDC(pWnd), getWidth(), getHeight());
  //  mapDC->CreateCompatibleDC(new CClientDC(pWnd));
  //  
  //  for (int i = 0; i < rows; i++) {
  //    cells[i].print(mapDC, i, i);
  //  }
  //}

private:

  void setWidthOfCells(int);

};
