#include "StdAfx.h"
#include "Offscreen.h"

Offscreen::Offscreen(CDC *pDC, size_t width, size_t height):
  nWidth(width),
  nHeight(height)
{
  cdcCompatible = new CDC();
  cdcCompatible->CreateCompatibleDC(pDC);

  cBitmap = new CBitmap();
  cBitmap->CreateCompatibleBitmap(pDC, nWidth, nHeight);

  cBitmapOld = cdcCompatible->SelectObject(cBitmap);
  cdcCompatible->PatBlt(0, 0, nWidth, nHeight, WHITENESS);
}

Offscreen::~Offscreen(void)
{
  delete cdcCompatible;
  delete cBitmap;  
}

void Offscreen::write(Printable *pPrintable) {
  pPrintable->print(cdcCompatible);
}

void Offscreen::print(CDC *pDC, LPRECT lpRect) {
  pDC->BitBlt(lpRect->left, lpRect->top, lpRect->right, lpRect->bottom, cdcCompatible, 0, 0, SRCCOPY);
}

void Offscreen::stretch(CDC *pDC, int width, int height) {
  pDC->StretchBlt(0, 0, width, height, cdcCompatible, 0, 0, nWidth, nHeight, SRCCOPY);

#ifdef _DEBUG
  printf("%4d %4d %4d %4d \n", width, height, nWidth, nHeight);
#endif
}